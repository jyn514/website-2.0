#!/bin/sh

set -euv

usage() {
	echo "usage: deploy-git.sh <message> <branch>"
	exit 1
}

# you can set a custom commit message with `./deploy-git.sh message`
if [ "$#" -ge 1 ]; then
	MESSAGE="$1"
else
	usage
fi

# you can set the target branch with `./deploy-git.sh message branch`
if [ "$#" -ge 2 ]; then
	BRANCH="$2"
else
	usage
fi

# this is the directory where the HTML is stored; ./public by default
HTML_DIR="$(git rev-parse --show-toplevel)/public"

cd "$HTML_DIR"

# does nothing if .git already exists
git init
# on the first run, add the repo
if ! git remote | grep -q origin; then
	git remote add origin git@gitlab.com:uofscacm/website/html-output.git
fi

git fetch --depth 2
# if the branch already exists, stay up to date
if git branch --all --verbose | grep -q origin/"$BRANCH"; then
	git reset origin/"$BRANCH" --
fi

# configure name and email if they don't exist already
if ! git config -l | grep -q user.email; then
	git config user.email '<autodeploy@example.com>'
fi
if ! git config -l | grep -q user.name; then
	git config user.name 'GitLab autodeploy runner'
fi

git add .
# don't do anything if there are no changes
if git diff-index --quiet HEAD; then
	exit 0
fi
git commit --message "$MESSAGE"
git push --set-upstream origin HEAD:"$BRANCH"
