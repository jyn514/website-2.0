import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero, { heroOptions } from "../components/hero"
import Container from "../components/container"
import Link from "../components/link"

const AboutPage = () => (
  <Layout>
    <SEO title="Code of Conduct" />
    <Hero title="ACM@USC Code of Conduct" color={heroOptions.greyBlue} />
    <Container>
      <h2 id="why-have-a-code-of-conduct-">Why Have a Code of Conduct?</h2>
      <hr />
      <p>
        ACM@USC&#39;s community includes people from many different backgrounds.
        The ACM@USC contributors are committed to providing a friendly, safe,
        and welcoming environment for all, regardless of age, disability,
        gender, nationality, race, religion, sexuality, or similar personal
        characteristic.
      </p>
      <p>
        The first goal of the Code of Conduct is to specify a baseline standard
        of behavior so that people with different social values and
        communication styles can communicate effectively, productively, and
        respectfully.
      </p>
      <p>
        The second goal is to provide a mechanism for resolving conflicts in the
        community when they arise.
      </p>
      <p>
        The third goal of the Code of Conduct is to make our community welcoming
        to people from different backgrounds. Diversity is critical in order for
        us to build a thriving community; for ACM@USC to be successful, it needs
        hackers from all backgrounds.
      </p>
      <p>
        With that said, a healthy community must allow for disagreement and
        debate. The Code of Conduct is not a mechanism for people to silence
        others with whom they disagree.
      </p>
      <h2 id="where-does-the-code-of-conduct-apply-">
        Where Does the Code of Conduct Apply?
      </h2>
      <hr />
      <p>
        If you join in or contribute to the ACM@USC ecosystem in any way, you
        are encouraged to follow the Code of Conduct while doing so.
      </p>
      <p>
        Explicit enforcement of the Code of Conduct applies to all official
        online ACM@USC groups, in-person meetings, and events, including:
      </p>
      <ul>
        <li>
          The{" "}
          <Link to="https://groupme.com/join_group/25721586/R5AtRy">
            ACM@USC GroupMe
          </Link>
        </li>
        <li>
          The <Link to="https://gitlab.com/uofscacm/">ACM@USC GitLab</Link>
        </li>
        <li>Weekly Meetings</li>
      </ul>
      <p>
        Other ACM@USC groups (such as hackathons, conferences, meetups, and
        other unofficial forums) are encouraged to adopt this Code of Conduct.
        Those groups must provide their own moderators and/or working group (see
        below).
      </p>
      <h2 id="hacker-values">Hacker Values</h2>
      <hr />
      <p>
        These are the values to which people in the ACM@USC community should
        aspire.
      </p>
      <ul>
        <li>Be friendly and welcoming</li>
        <li>
          Be patient
          <ul>
            <li>
              Remember that people have varying communication styles and that
              not everyone is using their native language (meaning and tone can
              be lost in translation).
            </li>
          </ul>
        </li>
        <li>
          Be thoughtful
          <ul>
            <li>
              Productive communication requires effort. Think about how your
              words will be interpreted.
            </li>
            <li>
              Remember that sometimes it is best to refrain entirely from
              commenting.
            </li>
          </ul>
        </li>
        <li>
          Be respectful
          <ul>
            <li>In particular, respect differences of opinion.</li>
          </ul>
        </li>
        <li>
          Be charitable
          <ul>
            <li>
              Interpret the arguments of others in good faith, do not seek to
              disagree.
            </li>
            <li>When we do disagree, try to understand why.</li>
          </ul>
        </li>
        <li>
          Avoid destructive behavior:
          <ul>
            <li>
              Derailing: stay on topic; if you want to talk about something
              else, start a new conversation.
            </li>
            <li>
              Unconstructive criticism: don&#39;t merely condemn the current
              state of affairs; offer—or at least solicit—suggestions as to how
              things may be improved.
            </li>
            <li>Snarking (pithy, unproductive, sniping comments)</li>
            <li>
              Discussing potentially offensive or sensitive issues; this all too
              often leads to unnecessary conflict.
            </li>
            <li>
              Microaggressions: brief and commonplace verbal, behavioral, and
              environmental indignities that communicate hostile, derogatory or
              negative slights and insults to a person or group.
            </li>
          </ul>
        </li>
      </ul>
      <p>
        People are complicated. You should expect to be misunderstood and to
        misunderstand others; when this inevitably occurs, resist the urge to be
        defensive or assign blame. Try not to take offense where no offense was
        intended. Give people the benefit of the doubt. Even if the intent was
        to provoke, do not rise to it. It is the responsibility of{" "}
        <em>all parties</em> to de-escalate conflict when it arises.
      </p>
      <h2 id="unwelcome-behavior">Unwelcome Behavior</h2>
      <hr />
      <p>These actions are explicitly forbidden in ACM@USC spaces:</p>
      <ul>
        <li>
          Expressing or provoking:
          <ul>
            <li>insulting, demeaning, hateful, or threatening remarks;</li>
            <li>
              discrimination based on age, nationality, race, (dis)ability,
              gender (identity or expression), sexuality, religion, or similar
              personal characteristic;
            </li>
            <li>bullying or systematic harassment;</li>
            <li>
              unwelcome sexual advances, including sexually explicit content.
            </li>
          </ul>
        </li>
        <li>
          Excessive advertisement for unnecessary or non-beneficial commercial
          products and services.
        </li>
        <li>
          Posting spam-like content that disrupts the environment of the
          community.
        </li>
      </ul>
      <h2 id="moderation-enforcement">Moderation &amp; Enforcement</h2>
      <hr />
      <p>
        Please understand that speech and actions have consequences, and
        unacceptable behavior will not be tolerated. When you participate in{" "}
        <Link to="#where-does-the-code-of-conduct-apply">
          areas where the code of conduct applies
        </Link>
        , you should act in the spirit of the &quot;Hacker values&quot;. If you
        conduct yourself in a way that is explicitly forbidden by the Code of
        Conduct, you will be warned and asked to stop, and your messages may be
        removed by community moderators. Repeated offenses may result in a
        temporary or permanent ban from the community.
      </p>
      <ul>
        <li>
          On your first offense, you will receive a written notice from one of
          our community moderators. Depending on the degree of the reported
          behavior, you may be asked to apologize, either in public or directly
          to the party that you have offended.
        </li>
        <li>
          On a second offense, you will be temporarily removed from the
          community. The period of the temporary ban may vary from 3 days to a
          month, decided based on the seriousness of the reported behavior.
          Please note that this ban{" "}
          <strong>
            does not indicate that you are no longer welcomed in the community
          </strong>{" "}
          - it represents an official warning for your behavior.
        </li>
        <li>
          On a third offense, you may be asked to leave the community. Your
          account may be suspended for an indefinite amount of time, and you may
          be publicly identified.
        </li>
      </ul>
      <p>
        This procedure only serves as a general guideline for moderation &amp;
        enforcement of our community conduct. Under all circumstances, the
        Working Group or ACM@USC&#39;s staff members may take any action we deem
        appropriate, including immediate removal from the community. Being
        banned from the ACM@USC community may also prevent you from
        participating in our community events, including but not restricted to:
        local club meetings, hackathons, or challenges.
      </p>
      <p>
        Please understand that we will not restrict your ability to contact the{" "}
        <Link to="#working-group">Code of Conduct working group</Link> under any
        circumstance. If you have any questions or concerns about our decision,
        please reach out to us directly at{" "}
        <Link to="mailto:uofscacm@gmail.com">uofscacm@gmail.com</Link>.
      </p>
      <h2 id="working-group">Working Group</h2>
      <hr />
      <p>
        The Code of Conduct Working Group is a group of people that represent
        the ACM@USC community. They are responsible for handling conduct-related
        issues. Their purpose is to de-escalate conflicts and try to resolve
        issues to the satisfaction of all parties. They are:
      </p>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Justin Baum</td>
            <td>
              <Link to="mailto:jabaum@email.sc.edu">jabaum@email.sc.edu</Link>
            </td>
          </tr>
          <tr>
            <td>Joshua Nelson</td>
            <td>
              <Link to="mailto:jynelson@email.sc.edu">
                jynelson@email.sc.edu
              </Link>
            </td>
          </tr>
          <tr>
            <td>Dalton Craven</td>
            <td>
              <Link to="mailto:djcraven@email.sc.edu">
                djcraven@email.sc.edu
              </Link>
            </td>
          </tr>
          <tr>
            <td>William Hobbs</td>
            <td>
              <Link to="mailto:wihobbs@email.sc.edu">wihobbs@email.sc.edu</Link>
            </td>
          </tr>
          <tr>
            <td>David Schmitt</td>
            <td>
              <Link to="mailto:schmitd@email.sc.edu">schmitd@email.sc.edu</Link>
            </td>
          </tr>
        </tbody>
      </table>
      <h2 id="reporting-issues">Reporting Issues</h2>
      <hr />
      <p>
        If you encounter a conduct-related issue, you should report it to the
        Working Group using the process described below. <strong>Do not</strong>{" "}
        post about the issue publicly or try to rally sentiment against a
        particular individual or group.
      </p>
      <ul>
        <li>
          Email <Link to="mailto:uofscacm@gmail.com">uofscacm@gmail.com</Link>
          <ul>
            <li>Your message will reach the Working Group.</li>
            <li>Reports are confidential within the Working Group.</li>
            <li>
              Should you choose to remain anonymous then the Working Group
              cannot notify you of the outcome of your report.
            </li>
            <li>
              You may contact a member of the group directly if you do not feel
              comfortable contacting the group as a whole. That member will then
              raise the issue with the Working Group as a whole, preserving the
              privacy of the reporter (if desired).
            </li>
            <li>
              If your report concerns a member of the Working Group they will be
              recused from Working Group discussions of the report.
            </li>
            <li>
              The Working Group will strive to handle reports with discretion
              and sensitivity, to protect the privacy of the involved parties,
              and to avoid conflicts of interest.
            </li>
          </ul>
        </li>
        <li>
          You should receive a response within 48 hours (likely sooner). (Should
          you choose to contact a single Working Group member, it may take
          longer to receive a response.)
        </li>
        <li>
          The Working Group will meet to review the incident and determine what
          happened.
          <ul>
            <li>
              With the permission of person reporting the incident, the Working
              Group may reach out to other community members for more context.
            </li>
            <li>
              The Working Group will reach a decision as to how to act. These
              may include:
              <ul>
                <li>Nothing.</li>
                <li>A request for a private or public apology.</li>
                <li>A private or public warning.</li>
                <li>
                  An imposed vacation (for instance, asking someone to abstain
                  for a week from group events).
                </li>
                <li>
                  A permanent or temporary ban from some or all ACM@USC spaces.
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          The Working Group will reach out to the original reporter to let them
          know the decision.
        </li>
        <li>
          Appeals to the decision may be made to the Working Group, or to any of
          its members directly.
        </li>
      </ul>
      <p>
        <strong>
          Note that the goal of the Code of Conduct and the Working Group is to
          resolve conflicts in the most harmonious way possible.
        </strong>{" "}
        We hope that in most cases issues may be resolved through polite
        discussion and mutual agreement. Bans and other forceful measures
        are to be employed only as a last resort.
      </p>
      <p>
        Changes to the Code of Conduct (including to the members of the Working
        Group) should be proposed by creating an issue or making a pull request
        to this document.
      </p>
      <h2 id="summary">Summary</h2>
      <hr />
      <ul>
        <li>Treat everyone with respect and kindness.</li>
        <li>Be thoughtful in how you communicate.</li>
        <li>Don&#39;t be destructive or inflammatory.</li>
        <li>
          If you encounter an issue, please mail{" "}
          <Link to="mailto:uofscacm@gmail.com">uofscacm@gmail.com</Link>.
        </li>
      </ul>
      <h2 id="acknowledgments">Acknowledgments</h2>
      <hr />
      <p>
        This was adapted from{" "}
        <Link to="https://hackclub.com/conduct">
          Hack Club&#39;s Code of Conduct
        </Link>
        . It is to be noted that many parts of Hack Club&#39;s Code of Conduct
        are adopted from the Code of Conduct documents of the Go, Django,
        FreeBSD, and Rust projects.
      </p>
    </Container>
  </Layout>
)

export default AboutPage
