import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero, { heroOptions } from "../components/hero"
import Container from "../components/container"
import Link from "../components/link"

const LinksPage = () => (
  <Layout title="Cool Links">
    <SEO title="Cool Links" />
    <Hero title="Cool Links" color={heroOptions.greenYellow} />
    <Container>
      <h2>Personal Blogs of Members</h2>

      <p>These links are from current and past members of ACM@USC.</p>

      <p>
        <Link to="http://cdaniels.net/">The Blog of Charles Daniels</Link>{" "}
        &mdash; Charles Daniels
      </p>

      <p>
        <Link to="https://nglaeser.github.io/">Circle Limit</Link> &mdash; Noemi
        Glaser
      </p>

      <p>
        <Link to="https://www.clayscode.com/">Clay&#39;s Code</Link> &mdash;
        Clay Norris
      </p>

      <p>
        <Link to="https://jaday.io/">Jeremy Day&#39;s Blog</Link> &mdash; Jeremy
        Day
      </p>

      <p>
        <Link to="https://jyn514.github.io/">Joshua Nelson&#39;s Blog</Link>{" "}
        &mdash; Joshua Nelson
      </p>

      <p>
        <Link to="https://kelevra.io/">Kelevra</Link> &mdash; Kevin Madison
      </p>

      <p>
        <Link to="http://thespicystew.com/">The Spicy Stew</Link> &mdash; Kenny
        Johnson
      </p>

      <p>
        <Link to="https://justinba1010.github.io/">
          Justin Baum&#39;s Personal Website
        </Link>{" "}
        &mdash; Justin Baum
      </p>

      <p>
        <Link to="http://arsmachina.net">Art of the Computer Machine</Link>{" "}
        &mdash; Brady O&#39;Leary
      </p>

      <p>
        <Link to="https://daltoncraven.me">Dalton's Personal Website</Link>{" "}
        &mdash; Dalton Craven
      </p>

      <h2>USC Websites</h2>

      <p>These links are to USC-related Websites.</p>

      <p>
        <Link to="https://www.usccyber.org/">USC Cybsersecurity</Link> &mdash;
        USC Cybersecurity Club
      </p>

      <p>
        <Link to="https://www.sc.edu/about/offices_and_divisions/student_success_center/supplemental&mdash;instruction/index.php">
          Supplemental Instruction
        </Link>{" "}
        &mdash; Student Success Center
      </p>

      <p>
        <Link to="https://www.cse.sc.edu/job">Jobs Board</Link> &mdash;
        Department of Computer Science and Engineering
      </p>

      <h2>Other Websites</h2>

      <p>These are other websites.</p>

      <p>
        <Link to="https://news.ycombinator.com/">Hacker News</Link> &mdash; News
        for Hackers
      </p>

      <p>
        <Link to="https://www.fsf.org/">Free Software Foundation</Link> &mdash;
        The Free Software Foundation Website
      </p>

      <p>
        <Link to="https://www.gnu.org/">GNU</Link> &mdash; The GNU Project&#39;s
        Website
      </p>

      <p>
        <Link to="https://www.eff.org/">EFF</Link> &mdash; The Electronic
        Frontier Foundation
      </p>

      <p>
        <Link to="https://www.openbsd.org/">OpenBSD</Link> &mdash; OpenBSD
        Website
      </p>

      <p>
        <Link to="http://www.minix3.org/">MINIX</Link> &mdash; MINIX Website
      </p>

      <p>
        <Link to="https://web.archive.org/web/*/http://acm.cse.sc.edu/">
          Old ACM Website
        </Link>{" "}
        &mdash; Archived Version of the old ACM Websites from 2002 to 2020.
      </p>

      <p>
        <Link to="https://docs.google.com/spreadsheets/d/1PuWnWZu-actrV9GtHhOu5Xr51Yx2myQAC9xbovcukLc/edit#gid=0">
          Textbooks Sale Spreadsheet
        </Link>
      </p>
    </Container>
  </Layout>
)

export default LinksPage
