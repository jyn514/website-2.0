import React from "react"
import { graphql } from "gatsby"
import styled from "@emotion/styled"

import Container from "../components/container"
import Layout from "../components/layout"
import { StyledLink } from "../components/link"
import SEO from "../components/seo"
import Hero, { heroOptions } from "../components/hero"

const Pagination = styled.nav`
  display: flex;
  justify-content: center; /* For horizontal alignment */
  background-image: none;

  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }

  li {
    display: inline-block;
    margin: 0;
  }

  a {
    padding: 15px 20px;
  }
`
export default class EventList extends React.Component {
  render() {
    const posts = this.props.data.allMarkdownRemark.edges
    const { currentPage, numPages } = this.props.pageContext
    const isFirst = currentPage === 1
    const isLast = currentPage === numPages
    const nextPage = (currentPage + 1).toString()

    return (
      <Layout>
        <SEO title="Home" />
        <Hero
          color={heroOptions.default}
          title="Recent News & Events"
          details={!isFirst && `Page ${currentPage} of ${numPages}`}
        />

        <Container>
          {posts.map(({ node }, index) => {
            return <EventEntry event={node} key={node.id} />
          })}
          <Pagination role="navigation" aria-label="Pagination Navigation">
            <ul>
              {!isFirst && (
                <li>
                  <StyledLink
                    to={`/${
                      currentPage - 1 === 1 ? "" : currentPage - 1 + "/"
                    }`}
                    rel="prev"
                    aria-label={`Go to Page ${
                      currentPage - 1 === 1 ? "1" : currentPage - 1
                    }`}
                  >
                    ← Previous Page
                  </StyledLink>
                  {"\u00A0"}
                </li>
              )}
              {Array.from({ length: numPages }, (_, i) => (
                <li key={i}>
                  <StyledLink
                    to={`/${i === 0 ? "" : i + 1 + "/"}`}
                    activeClassName="active"
                    aria-label={
                      i + 1 === currentPage
                        ? "Current page, page " + (i + 1)
                        : "Go to page " + (i + 1)
                    }
                  >
                    {i + 1}
                  </StyledLink>
                  {"\u00A0"}
                </li>
              ))}
              {!isLast && (
                <li>
                  <StyledLink
                    to={`/${nextPage}/`}
                    rel="next"
                    aria-label={`Go to Page ${currentPage + 1}`}
                  >
                    Next Page →
                  </StyledLink>
                </li>
              )}
            </ul>
          </Pagination>
        </Container>
      </Layout>
    )
  }
}

const EventEntry = ({ event }) => {
  return (
    <article>
      <h3>
        <StyledLink to={event.fields.slug}>
          {event.frontmatter.title.replace(/<[^>]+>/g, "")}
        </StyledLink>
      </h3>
      <p>
        <em>
          <time>
            {event.frontmatter.date}, {event.frontmatter.time}
          </time>
          {event.frontmatter.location && (
            <> &mdash; {event.frontmatter.location}</>
          )}
        </em>
      </p>
      <p
        dangerouslySetInnerHTML={{
          __html: event.frontmatter.summary,
        }}
      />
      <hr />
    </article>
  )
}

export const blogListQuery = graphql`
  query blogListQuery($skip: Int!, $limit: Int!) {
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            time
            location
            summary
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
