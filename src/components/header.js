import React from "react"
import styled from "@emotion/styled"

import { StyledLink } from "../components/link"

const Header = styled.header`
  display: none;
  text-align: center;

  h1 {
    margin: 40px;
  }

  p {
    color: ${({ theme }) => theme.accent};
  }

  @media screen and (min-width: 768px) {
    display: block;
  }
`

const Logo = styled(StyledLink)`
  font-size: 4rem;
  font-weight: bold;
  margin-bottom: 5px;
`

const SkipNav = styled.a`
  position: absolute;
  left: -10000px;
  top: auto;
  width: 1px;
  height: 1px;
  overflow: hidden;
  :hover,
  :focus {
    position: static;
    width: auto;
    height: auto;
  }
`

export default ({ title }) => {
  return (
    <>
      <SkipNav href="#main">Skip to main content</SkipNav>
      <Header>
        <h1>
          <Logo to="/">{title}</Logo>
        </h1>
        <p>
          Association for Computing Machinery at the University of South
          Carolina
        </p>
      </Header>
    </>
  )
}
