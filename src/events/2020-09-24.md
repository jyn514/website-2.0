---
title: Working on the Web; an Intro to Web Development & Design Part 1
date: 2020-09-24
time: 7:00 PM
location: Discord
author: Dalton Craven
summary: "An interactive lesson through the world of developing pretty and React-ive websites."
---

This will be an interactive lesson given by Dalton Craven. We will be using VSCode to break up into groups and pair program. This is a part 1 of 2 parts interactive pair programming exercise. If you were unable to come to our talk last year that was a interactive lesson, I highly recommend coming to this one. This talk will go over the basics of how/what a website should look and be.
