---
title: Virtual Ice Cream Social
date: 2020-10-22
time: 7:00 PM
location: Discord
author: David Schmitt
summary: "Bring ice cream."
---

Before GPUs were widespread most graphical applications used software rendering, that is rendering with the CPU. Even today, where GPUs are widespread, the CPU is used for some tasks, like occlusion culling, to reduce the load on the GPU. In this talk, we will be creating a simple software renderer from scratch in order to gain a better understanding of concepts in computer graphics.
