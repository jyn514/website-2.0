---
title: Functional Languages and Type Systems
date: 2020-10-09
time: 7:00 PM
author: Justin Baum
location: Discord
summary: "Why use a paradigm that is unwieldy?"
---

We will discuss referential transparency, immutability, and its contrast with object oriented languages. We will also talk briefly about type safety and why using languages like Rust, Haskell, OCaml, Go, and others can bring a lot of utility above C++ and others. Today many languages are multi-paradigm and even languages like Python support many functional language capabilities, and it is important to have the ability to use functional techniques in modern software engineering.
