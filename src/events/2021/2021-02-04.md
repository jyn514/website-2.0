---
title: Intro to Rust
date: 2021-02-04
time: 7:00 PM
location: Discord
author: Joshua Nelson
summary: "What is Rust? Where is Rust? Why is Rust? An introduction to the Rust programming language."
---

This week, Joshua Nelson will be speaking about the Rust programming language:
its strengths, weaknesses, and why many enterprise companies are using it and
funding its development. Along the way, he will explain how Rust was created by a secret cabal of Haskell developers to teach programmers monads, how C++ is hampered by backwards compatibility, and why Rust can be easily compiled to WASM.

---

The slides for this talk [are available](/assets/2021-02-04-intro-rust/index.html). This talk was also [recorded on YouTube](https://youtu.be/7hlnR2u253A).
