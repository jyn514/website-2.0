---
title: "Guest Speaker: Dr. Biplav Srivastava on Chatbots"
date: 2019-10-30
time: 5:00 PM
location: 300 Main B103
author: Dr. Biplav Srivastava
tags:
  - "Guest Speaker"
summary: The Potential and Risks of Working With Conversation Agents ('Chatbots')
---

This week we are excited to host ACM Distinguished Speaker, [Dr. Biplav Srivastava](https://researcher.watson.ibm.com/researcher/view.php?person=us-biplavs), for a lecture titled "The Potential and Risks of Working With Conversation Agents ('Chatbots')".
This event is hosted with the help of the [Artificial Intelligence Institute](http://ai.sc.edu).

Note: this talk will _not_ be in our normal meeting location,
it will be in 300 Main instead.

## Abstract

---

From the very start of Artificial Intelligence (AI), performing natural conversation has been a key pursuit of AI research and development. Their most recent form, chatbots, which can engage people in natural conversation and are easy to build in software, have been in the news a lot lately. There are many platforms to create dialogs quickly for any domain, based on simple rules. Further, there is a mad rush by companies to release chatbots to show their AI capabilities and gain market valuation. However, beyond basic demonstration, there is little experience in how they can be designed and used for real-world applications that need decision making under constraints (e.g., sequential decision making), work with groups of people and deal with dynamic data. Further, users expect systems to adapt their functionality to their users’ individual needs, convincingly explain their suggestions and decision-making behavior, and maintain highest ethical standards.

This talk will summarize the area of task-oriented conversation agents and highlight key considerations while selecting, designing, building, deploying and maintaining them. The talk will be agnostic to any company's offering and will be relevant to researchers and professionals building user-facing, data-driven, decision-support technologies.

## Bio

---

Biplav Srivastava is a Distinguished Data Scientist and Master Inventor at IBM's Chief Analytics Office. With over two decades of research experience in Artificial Intelligence, Services Computing and Sustainability, most of which was at IBM Research, Biplav is also an ACM Distinguished Scientist and Distinguished Speaker, and IEEE Senior Member. Biplav’s business focus is on promoting adoption of AI technologies in a large-scale global business context and understanding their impact on workforce. Technically, he focuses on promoting goal-oriented, ethical, human-machine collaboration via natural interfaces using domain and user models, learning, and planning.

Biplav received Ph.D. in 2000 and M.S. in 1996 from Arizona State University, USA and B.Tech. in 1993 from IIT-BHU, India, all in Computer Science. He actively participates in professional services globally including running the ‘AI in India’ virtual Google group with ~200 members since 2010; organizing conference tracks, workshops and tutorials, and as a Program Committee member for more than 50 events; and participating in Work Groups of international organizations like World Wide Web Consortium (W3C) and Partnership on AI (PAI).
