import java.util.ArrayList;

class Main {
  public static void main(String[] args) {
    ArrayList<String> my_list = new ArrayList<>();
    my_list.add("hello");
    assert pop(my_list) == "hello";
    my_list.add("goodbye");
    assert pop(my_list) == "goodbye";
  }

  public static String pop(ArrayList<String> list) {
    if (list.size() == 0) {
      return null;
    } else {
      return list.remove(list.size() - 1);
    }
  }

  public static <T> T pop_generic(ArrayList<T> list) {
    if (list.size() == 0) {
      return null;
    } else {
      return list.remove(list.size() - 1);
    }
  }

  public static int length(CharSequence seq) {
    return seq.length();
  }
}
