---

## Type Erasure

- Mainly used by Java (not sure if there are any other languages?)
- Only works on pointers

```
import java.util.ArrayList;

class Main {
  public static void main(String[] args) {
    ArrayList<int> list = new ArrayList<>();
  }
}
non-pointer-generics.java:5: error: unexpected type
    ArrayList<int> list = new ArrayList<>();
              ^
  required: reference
  found:    int
1 error
```

- Works at runtime: everything is boxed, so it's all the same [size]
- Workaround for primitives: box them!

For example, [Integer]

- Can be compiled ahead of time

You can write and compile a .class file that works for types *you don't know about yet*.

[size]: https://doc.rust-lang.org/book/ch19-04-advanced-types.html?#dynamically-sized-types-and-the-sized-trait
<!-- intentionally unused links -->
[stack-heap]: https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html#the-stack-and-the-heap
[memory-bugs]: https://acm.cse.sc.edu/events/2020-04-23/
[Integer]: https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html

---

## Monomorphization

- Used by C++, D, Rust
- Works on [any type], not just pointer types
- Cannot be compiled ahead of time
  - 'Template hell' - this is why C++ is slow to compile
  - Pre-compiled headers (misnomer) - compare `.rlib`s in Rust
- Results in more efficient code at run time, but also _more_ code.

In particular, there's a separate copy of the function for each type.

[any type]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=466de4997cd86abd958c933633bf6e36

---

## Duck typing

- Used by Python, JavaScript
- If there's no types, no need to say which types the function works for!
- Works similarly to type erasure, but not checked at compile time

```python
def pop(list):
  list.pop()
```

- You can replicate duck typing in Java with reflection.

This is not recommended, though.

See [example with add](https://jyn514.github.io/2018/06/09/Rewriting-Jython.html)
if you really want to know (it's ugly).

```java
        public double add(Object t) throws ReflectiveOperationException {
            return new 1.0
                   + (double) t.getClass().getMethod("doubleValue").invoke(t));
        }
```

---

# Restricting types

- Everything so far works on arbitrary types
- Most flexible; also very limited

How do you add two numbers generically? ([playground](https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=4ddf2e8c44ce49fed42783cb2d489f59))

```rust
pub fn add<T>(a: T, b: T) -> T {
    a + b
}
```

- Need to declare 'properties' of the type

## Interfaces

- Similar to type erasure: variables are boxed
- 'This function takes this interface'

[More info](https://docs.oracle.com/javase/tutorial/java/concepts/interface.html)

```java
public static int length(CharSequence seq) {
  return seq.length();
}
```

- Go calls them interfaces, so it's getting lumped in with Java

Not entirely sure if this is boxed or not.

```go
func MyAbs(a Abser) float64 {
  return a.Abs()
}
```

## Traits and Type-classes

- 'This function takes a specific type that _implements_ an interface'
  - Rust interfaces are called 'traits'.
  - Haskell interfaces are called 'type classes'.

- Rust example

[More info](https://doc.rust-lang.org/book/ch10-00-generics.html)

```rust
pub fn add<T: std::ops::Add<Output = T>>(a: T, b: T) -> T {
    a + b
}
```

- Haskell example

[More info](http://learnyouahaskell.com/types-and-typeclasses)

```haskell
add :: (Num a) => a -> a -> a
add a b = a + b
```

- C++ has these, but ... weird

This works _exactly_ when T has `operator +`

```c++
template<typename T> auto add(T a, T b) -> decltype(a + b) {
    return a + b;
}
```

- But if there's no operator, there's not necessarily an error!
// This version works only with strings
void add(const char *a, const char *b) {
    return;
}
```

- Duck typing, but at compile time!

This is called '[SFINAE]': Substitution Failure Is Not An Error.

[SFINAE]: https://en.cppreference.com/w/cpp/language/sfinae
